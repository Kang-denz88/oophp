<?php 
  require_once("animal.php");
  require_once("frog.php");
  require_once("sungokong.php");
  $animal = new Animal("Shaun");
  echo "Nama Animal = " . $animal->nama . "<br>" ;
  echo "Jumlah Legs-Nya adalah = " . $animal->legs . "<br>" ;
  echo "Cold Blooded-Nya adalah = " . $animal->cold_blooded . "<br><br>" ;

  $kodok = new Frog("Buduk");
  echo "Nama Animal = " . $kodok->nama . "<br>" ;
  echo "Jumlah Legs_nya adalah = " . $kodok->legs . "<br>" ;
  echo "Cold Blooded-Nya adalah = " . $kodok->cold_blooded . "<br>" ;
  echo $kodok->jump() . "<br><br>" ;

  $sungokong = new Ape("Kera Sakti");
  echo "Nama Animal = " . $sungokong->nama . "<br>" ;
  echo "Jumlah Legs_nya adalah = " . $sungokong->legs . "<br>" ;
  echo "Cold Blooded-Nya adalah = " . $sungokong->cold_blooded . "<br>" ;
  echo $sungokong->yell();
?>